/*
  ==============================================================================

    graphics.cpp
    Created: 25 Jan 2023 8:47:33pm
    Author:  faure

  ==============================================================================
*/

#include "Source/editor/graphics.h"


//============================
TCanvas::TCanvas(const char* name,  int cx_i, int cy_i, int wx_i, int wy_i)
{

//------------------
	cx = cx_i;
	cy = cy_i;
	
	wx = wx_i;
	wy = wy_i;

	 
	

	title = name;
}

//=====================
TCanvas::~TCanvas()
{
}


//=================
void TCanvas::Range(double xmin_, double ymin_, double xmax_, double ymax_)
{
		xmin = xmin_; ymin = ymin_; xmax = xmax_; ymax = ymax_; 

}


//==========
void TCanvas::Clear()
{
	
	
}
//=============================
void TCanvas::SetFillColor( Graphics &g, int col_)
{
	g.fillAll (Colour ((uint) col_));
}

//=================================
void TCanvas::DrawFrame(double xmin_, double ymin_, double xmax_, double ymax_)
{
	Range(xmin_,ymin_, xmax_, ymax_);
}

//------------------------
// convert coordinates
void TCanvas::convert_xy_to_pxpy(double x,double y, int &px, int &py)
{
	px = (x-xmin)/(xmax-xmin)*wx;
	py = wy - (y-ymin)/(ymax-ymin)*wy;
}
//--------------------------
// convert distances
void TCanvas::convert_dx_to_dpx(double dx, int &dpx)
{
	dpx = dx/(xmax-xmin)*wx;
}
//--------------------------
// convert distances
void TCanvas::convert_dy_to_dpy(double dy, int &dpy)
{
	dpy = dy/(ymax-ymin)*wy;
}

//------------------------
// convert coordinates
void TCanvas::convert_pxpy_to_xy(int px, int py, double &x,double &y)
{
	x = AbsPixeltoX(px);
	y = AbsPixeltoY(py);
}

//---------------------------------
double TCanvas::AbsPixeltoX(int px)
{
	return 	px/((double) wx) *(xmax-xmin)+xmin;
}
//---------------------------------
double TCanvas::AbsPixeltoY(int py)
{
	return 	(wy-py)/((double) wy) *(ymax-ymin) + ymin;
}

//=======================
void TCanvas::SetTitle(const char * title_)
{
	title = title_;
}




//========================================================
TLine::TLine(double x1_, double y1_, double x2_, double y2_)
{
	x1=x1_; y1=y1_; x2=x2_; y2=y2_;
}

void TLine::SetLineColor(int col_)
{
	col=col_;
}

void TLine::SetLineStyle(int style_)
{
	style=style_;
}

void TLine::SetLineWidth(int w_)
{
	w=w_;
}
//------------------------------------
void TLine::Draw(TCanvas &c, Graphics &g)
{
    int px1,py1;
     c.convert_xy_to_pxpy(x1, y1, px1, py1);
    int px2,py2;
     c.convert_xy_to_pxpy(x2, y2, px2, py2);
     
     
	 g.setColour(Colour((uint)col));


		
	if(style == 1) // full
		g.drawLine(px1, py1, px2, py2, w);
	else 	if(style == 2) // dashed2
	{
		std::vector<float> dashLengths = { 4.0f, 4.0f}; // pixels for dashed (full, gap)
		g.drawDashedLine(Line((float)px1, (float)py1, (float)px2, (float)py2), dashLengths.data() , 2, w);
	}
	else 	if(style == 3) // dashed3
	{
		std::vector<float> dashLengths = { 10.0f, 2.0f}; // pixels for dashed (full, gap)
		g.drawDashedLine(Line((float)px1, (float)py1, (float)px2, (float)py2), dashLengths.data() , 2, w);
	}
		
/*
    if(px1 == px2) // vertical line
    {
      if(py2 >= py1)
          g.fillRect(px1, py1, w, py2-py1);
      else
          g.fillRect(px2, py2, w, py1-py2);
    }
	else if(py1 == py2) // horizontal line
    {
      if(px2 >= px1)
          g.fillRect(px1, py1,  px2 - px1, w);
      else
          g.fillRect(px2, py2, px1-px2, w);
    } 	
*/
/*	
    if(px1 == px2) // vertical line
    {
		if(py1 > py2)
			swap(py1, py2);
		
		g.drawVerticalLine(px1, py2, py1); 
    }
	else if(py1 == py2) // horizontal line
    {
		if(px1 > px2)
			swap(px1, px2);
		g.drawHorizontalLine(py1, px2, px1);
    } 	
	else // general line
	{
*/

//	}
	
//		g.drawVerticalLine(10, 20, 100);
}

//---------------------
// en coordonnees 0->1 avec (0,0) en bas a gauche
void TLine::DrawLineNDC(TCanvas &c, Graphics &g, double X1, double Y1, double X2, double Y2)
{
	x1 = c.xmin + (c.xmax - c.xmin)*X1;
	y1 = c.ymin + (c.ymax - c.ymin)*Y1;

	x2 = c.xmin + (c.xmax - c.xmin)*X2;
	y2 = c.ymin + (c.ymax - c.ymin)*Y2;

	Draw(c,g);
	
}

//================================
void TLine::Delete()
{

	
	delete this;
}


//========================================================
TPave::TPave()
{
}

//========================================================
TPave::TPave(double x1_, double y1_, double x2_, double y2_)
{
	x1=x1_; y1=y1_; x2=x2_; y2=y2_;
}
//==========================================
void TPave::SetLineColor(int col_)
{
	col=col_;
}

//==========================================
void TPave::SetFillColor(int fcol_)
{
	fcol=fcol_;
}
//==========================================
void TPave::SetLineStyle(int style_)
{
	style=style_;
}
//==========================================
void TPave::SetLineWidth(int s)
{
	size = s;
}
//------------------------------------
void TPave::Draw(TCanvas &c, Graphics &g)
{

    int px1,py1;
	c.convert_xy_to_pxpy(x1, y1, px1, py1);
    int px2,py2;
	c.convert_xy_to_pxpy(x2, y2, px2, py2);
     
    if(px1 > px2)
		swap(px1,px2);
	
	if(py1 > py2)
		swap(py1,py2);

	int w = px2 - px1;
	int h = py2 - py1;
	
	if(fcol != 0)
	 {
		 g.setColour(Colour((uint)fcol)); 
		 g.fillRect(px1, py1, w, h);
     }
	 if(col != 0)
	 {
		 g.setColour(Colour((uint)col)); 
		 g.drawRect(px1, py1, w, h, size);
	 }

	 
}

//---------------------
// en coordonnees 0->1 avec (0,0) en bas a gauche
void TPave::DrawNDC(TCanvas &c, Graphics &g, double X1, double Y1, double X2, double Y2)
{
	x1 = c.xmin + (c.xmax - c.xmin)*X1;
	y1 = c.ymin + (c.ymax - c.ymin)*Y1;

	x2 = c.xmin + (c.xmax - c.xmin)*X2;
	y2 = c.ymin + (c.ymax - c.ymin)*Y2;

	Draw(c,g);
	
}


//================================
void TPave::Delete()
{

	
	delete this;
}


//==============================
TLatex::TLatex(double x_, double y_, const char * s_)
{
	x=x_;
	y=y_;
	text = s_;
}
//============================
void TLatex::SetTextSize(double size_i)
{
	size  = size_i;
}
//=======================
void	TLatex::SetTextColorAlpha(int col_i, double transp_i)
{
	col = col_i;
	transp_i = transp;
}

//=======================
void	TLatex::SetTextColor(int col_i)
{
	col = col_i;

}
//-----------------
void TLatex::SetTextAlign(int align_)
{
	align = align_;
}
//=====================
void TLatex::Draw(TCanvas &c, Graphics &g)
{
     int px, py;
     c.convert_xy_to_pxpy(x, y, px, py);

     int w = c.wx * size * text.size();
     int h = c.wy * size;

	 if(align == 22) // centered
	 {
		 px = px - w/2;
		 py = py - h/2;
	 }
	 else
	 {
		 py = py - h;
	 }	 
	 
	 
//	 cout<<"px="<<px<<" py="<<py<<" w="<<w<<" h="<<h<<" text="<<text<<endl;
	 
	 g.setColour(Colour((uint)col));
	 g.setFont(c.wx * size);
	 // g.setFont (juce::Font ("Times New Roman", c.wx * size, juce::Font::italic));


	 
	 // left = 1 , right = 2 , horizontallyCentred = 4 , top = 8 ,
	 // bottom = 16 , verticallyCentred = 32 , horizontallyJustified = 64 ,
	 // centred = 36 , centredLeft = 33 , centredRight = 34 , centredTop = 12 ,
	 // centredBottom = 20 , topLeft = 9 , topRight = 10 , bottomLeft = 17 , bottomRight = 18 

	 if(align == 22)
		 g.drawText (text,  px, py, w, h, 36  , 1); //1: ... are drawn if text is too long
	 else
		 g.drawText (text,  px, py, w, h, 17 , 1); //1: ... are drawn if text is too long
	           
}
//---------------
void TLatex::DrawLatexNDC(TCanvas &c, Graphics &g, double X, double Y, const char * text_)
{
	x = c.xmin + (c.xmax - c.xmin)*X;
	y = c.ymin + (c.ymax - c.ymin)*Y;

	text = text_;
	
	Draw(c,g);
	
}

//================================
void TLatex::Delete()
{

	
	delete this;
}

//========================================================
TMarker::TMarker(double x_i, double y_i, int style_i)
{
	x=x_i; y=y_i; style=style_i;
}

//========================================================
void TMarker::SetMarkerColor(int col_i)
{
	col = col_i;
}

//========================================================
void TMarker::SetMarkerStyle(int style_i)
{
	style = style_i;
}

//========================================================
void TMarker::SetMarkerSize(int size_i)
{
	size = size_i;
}
//=========================================
void TMarker::Draw(TCanvas &c, Graphics &g)
{

    int px, py;
	c.convert_xy_to_pxpy(x, y, px, py);
	 
	g.setColour(Colour((uint)col));

	 

	g.fillEllipse(px - size/2., py - size/2., size, size);
	
}
//================================
void TMarker::Delete()
{
	delete this;
}


//==========================================
TEllipse::TEllipse(double x_, double y_, double r1_, double r2_, double phimin_, double phimax_, double theta_)
{
	x =x_; y=y_; r1=r1_; r2=r2_; phimin=phimin_; phimax=phimax_; theta=theta_;

	if(r2 == -1)
		r2 = r1;
}

//==========================================
void TEllipse::SetLineColor(int col_)
{
	col=col_;
}

//==========================================
void TEllipse::SetLineStyle(int style_)
{
	style=style_;
}

//==========================================
void TEllipse::SetLineWidth(int w_)
{
	w=w_;
}

//==========================================
void TEllipse::SetFillColor(int fcol_)
{
	fcol=fcol_;
}

//==========================================
void TEllipse::SetR1(double r1_)
{
	r1 = r1_;
}

//==========================================
void TEllipse::SetR2(double r2_)
{
	r2 = r2_;
}

//==========================================
void TEllipse::Draw(TCanvas &c, Graphics &g)
{
    int px, py;
     c.convert_xy_to_pxpy(x, y, px, py);
	 int pr1, pr2;
	 c.convert_dx_to_dpx(r1, pr1);
	 c.convert_dy_to_dpy(r2, pr2);
	 
	 //... put  center to top-left corner
	 px = px - pr1;
	 py = py - pr2;


	 
//----  draw ordinary ellipse
	 if((fabs(phimax - phimin) + 1e-3) >= 2*M_PI)
	 {
		 if(fcol != 0)
		 {
			 g.setColour(Colour((uint)fcol)); 
			 g.fillEllipse(px, py, 2*pr1, 2*pr2); // description of the rectangle
		 }
		 if(col != 0)
		 {
			 g.setColour(Colour((uint)col)); 
			 g.drawEllipse(px, py, 2*pr1, 2*pr2, w); // description of the rectangle
		 }
	 }

//------ arc de cercle -----------------
	 else // arc de cercle
	 {
		 Path path;
		 path.addArc(px, py, 2*pr1, 2*pr2, -phimin + M_PI/2., -phimax + M_PI/2., true);
		 if(col != 0)
		 {
			 g.setColour(Colour((uint)col)); 
			 g.strokePath(path,  PathStrokeType (w));
		 }
	 }


	 
}
//================================
void TEllipse::Delete()
{

}


//==============================================
// r1, r2: intern/extern radius
TCrown::TCrown(double x_, double y_, double r1_, double r2_, double phimin_, double phimax_)
{
	x=x_;
	y=y_;
	r1=r1_;
	r2=r2_;
	phimin=phimin_;
	phimax=phimax_;
}

//==========================

void TCrown::SetLineColor(int col_)
{
	col=col_;
}
//===============================
void TCrown::SetFillColor(int fcol_)
{
	fcol=fcol_;
}
//================================
void TCrown::SetLineStyle(int style_)
{
	style = style_;
}
//==========================================
void TCrown::SetFillStyle(int fstyle_)
{
	fstyle = fstyle_;
}

//==========================================
void TCrown::SetLineWidth(int w_)
{
	w=w_;
}
//============================
void TCrown::Draw(TCanvas &c, Graphics &g)
{
	

	int px, py;
     c.convert_xy_to_pxpy(x, y, px, py);
	 int pr1, pr2;
	 c.convert_dx_to_dpx(r1, pr1);
	 c.convert_dy_to_dpy(r2, pr2);
	 
	 //... put  center to top-left corner
	 px = px - pr1;
	 py = py - pr2;
	 
	 if(fcol != 0)
	 {
		 g.setColour(Colour((uint)fcol)); 
		 g.fillEllipse(px, py, 2*pr1, 2*pr2);
     }
	 if(col != 0)
	 {
		 g.setColour(Colour((uint)col)); 
		 g.drawEllipse(px, py, 2*pr1, 2*pr2, w);
	 }
	

	
}


//================================
void TCrown::Delete()
{
	//......................
	
	delete this;
}



//===========================
/*
i0 >=0 : circular shift
 */
void  Draw_histo(TCanvas &c, Graphics &g, vector<double> &V,const char * xtitre,const char * ytitre, double xmin, double xmax, double ymin, double ymax,  const char * titre,  int i0)
{
	
	 g.fillAll(Colour((uint)kBlack));

	 c.Range(xmin, ymin, xmax, ymax);

	 double N = V.size();

	 TLine l;
	 l.SetLineColor(kRed);
	 l.SetLineWidth(3);
	 l.SetLineStyle(3);
		 
	 for(int i=0; i<V.size()-1; i++)
	 {

		 int j = i + i0;
		 
		 while(j >= V.size())
			 j -= V.size();
		 
		 l.x1 = i/N*(xmax-xmin) + xmin;
		 l.y1 = V[j];
		 l.x2 = (i+1)/N*(xmax-xmin) + xmin;
		 l.y2 = V[j+1];

		 //... for test: put in green big jumps
		 /*
		 if(fabs(l.y2-l.y1)>0.1) // big jump
		 {
			 l.SetLineColor(kGreen);
			 l.SetLineWidth(5);
		 }
		 else
		 {
			 l.SetLineColor(kRed);
			 l.SetLineWidth(3);
		 }
		 */
		 //......
		 l.Draw(c, g);
	 }// for i
	
}
