/*
  ==============================================================================

    graphics.h
    Created: 25 Jan 2023 8:47:33pm
    Author:  faure

    2023,

C++ Classes to do graphics with JUCE
As possible, these class have the same syntax as cern/root classes in c++.

  ==============================================================================
*/

#pragma once


#include "Source/JuceHeader.h"
using namespace juce;

#include <string>
using namespace std;


//=========================
#define kWhite (int)0xFFFFFFFF
#define kBlack (int)0xff000000
#define kRed (int)0xffff0000
#define kGreen (int)0xff00FF00
#define kGreen1 (int)0xff00D300
#define kGreen2 (int)0xff2F9938
#define kGreen3 (int)0xff105F17
#define kBlue (int)0xff0034F4
#define kYellow (int)0xFFFFFF00
#define kYellow2 (int)0xFFFFFF10
#define kPink (int)0xFFFF00FF
#define kViolet (int)0xFF8300FA
#define kSky (int)0xFF00DBFF
#define kGreen_gray (int)0xFF3BB1A3
#define kBlue_gray (int)0xFF799FB9
#define kGray (int)0xFFACACAC


#define kbase03    (int)0xff002b36
#define kbase02    (int)0xff073642
#define kbase01    (int)0xff586e75
#define kbase00    (int)0xff657b83
#define kbase0     (int)0xff839496
#define kbase1     (int)0xff93a1a1
#define kbase2     (int)0xffeee8d5
#define kbase3     (int)0xfffdf6e3
#define kyellow    (int)0xffb58900
#define korange    (int)0xffcb4b16
#define kred       (int)0xffdc322f
#define kmagenta   (int)0xffd33682
#define kviolet    (int)0xff6c71c4
#define kblue      (int)0xff268bd2
#define kcyan      (int)0xff2aa198
#define kgreen     (int)0xff859900

//========================================
/*
A Javascript canvas  that contains a JS Canvas

It will be in the main window.


Ref: DOM Window
 */
class TCanvas
{
public:
    TCanvas(const char * title_, int wx_, int wy_)  : TCanvas(title_, 0,0, wx_, wy_) {};
	TCanvas(const char * title="", int cx=0, int cy=0, int wx=100, int wy=100);
	~TCanvas();

	int cx,cy; //position on screen
	int wx,wy;//width of each (sub)canvas

	double xmin=0,xmax=1, ymin=0,ymax=1;
	
	string title ="";
	
	void Range(double xmin, double ymin, double xmax, double ymax);
	
	
	void DrawFrame(double xmin, double ymin, double xmax, double ymax);

	void convert_xy_to_pxpy(double x,double y, int &px, int &py);
	void convert_pxpy_to_xy(int px, int py, double &x,double &y);
	void convert_dx_to_dpx(double dx, int &dpx);
	void convert_dy_to_dpy(double dy, int &dpy);

	void SetTitle(const char* title);


	void Clear();
	void SetFillColor(Graphics &g, int col_);
	
	double AbsPixeltoX(int px);
	double AbsPixeltoY(int py);


			
};
//=========================


//=========================

class TLine
{
public:
	double x1=0,y1=0, x2=0, y2=0;

	TLine(double x1=0, double y1=0, double x2=0, double y2=0);
	int col=1;
	void SetLineColor(int col);
	int style = 1;
	void SetLineStyle(int style);
	int w=1;
	void SetLineWidth(int w);
	void Draw(TCanvas &c, Graphics &g);

//	int opt_NDC = 0;  // en coordonnees 0->1 avec (0,0) en bas a gauche
	void DrawLineNDC(TCanvas &c, Graphics &g, double x1, double y1, double x2, double y2);  // en coordonnees 0->1 avec (0,0) en bas a gauche
	void SetNDC(int opt);
	void Delete();
};


//=========================

class TPave
{
public:
	double x1=0,y1=0,x2=0,y2=0;
	TPave();
	TPave(double x1, double y1, double x2, double y2);

	int col = kYellow;
	void SetLineColor(int col);

	int fcol = 0; // 0 = no fill
	void SetFillColor(int fcol);

	int style = 1; // 1: full, 2: dashed
	void SetLineStyle(int style);
	
	int size = 1; // en pixels
	void SetLineWidth(int s);
	
	void Draw(TCanvas &c, Graphics &g);

//	int opt_NDC = 0;  // en coordonnees 0->1 avec (0,0) en bas a gauche
	void DrawNDC(TCanvas &c, Graphics &g, double x1, double y1, double x2, double y2);  // en coordonnees 0->1 avec (0,0) en bas a gauche
	void SetNDC(int opt);

	void Delete();
};


//=========================

class TLatex
{
public:
	double x=0,y=0;
	string text = "";
	TLatex(double x=0, double y=0, const char * ="");

	double size = 0.1; // 1 is the whole window
	void SetTextSize(double size);
	
	int col = 1;
	double transp = 0.5;
	void SetTextColorAlpha(int col, double transp);
	void SetTextColor(int col);

	int align = 11; //11: x,y are bottom left, 22: x,y are the center
	void SetTextAlign(int align=11);
	void Draw(TCanvas &c, Graphics &g);
	void DrawLatexNDC(TCanvas &c, Graphics &g, double x, double y, const char *);  // en coordonnees 0->1 avec (0,0) en bas a gauche
	void Delete();
};


//=========================

class TMarker
{
public:
	double x,y;

	TMarker(double x=0, double y=0, int style=8);

	int col=1;
	void SetMarkerColor(int col);

	int style = 8;	
	void SetMarkerStyle(int style);
	
	int size = 3; // en pixels
	void SetMarkerSize(int size);

	void Draw(TCanvas &c, Graphics &g);
	void Delete();
};


//=========================

class TEllipse
{
public:
	double x,y, r1,r2;
	double phimin, phimax, theta; // en radian
	TEllipse(double x, double y, double r1, double r2=-1, double phimin=0, double phimax=2*M_PI, double theta=0);

	void SetR1(double r1);
	void SetR2(double r2);
	
	int col=1; // -1 if no line 
	void SetLineColor(int col);

	int style = 1;
	void SetLineStyle(int style);

	int w=1;
	void SetLineWidth(int w);

	int fcol = 0; // -1 = no fill
	void SetFillColor(int fcol);

	void Draw(TCanvas &c, Graphics &g);
	void Delete();
};


//=========================

class TCrown
{
public:
	double x,y, r1,r2, phimin,phimax,theta;
	TCrown(double x, double y, double r1, double r2, double phimin=0, double phimax=360);
	int col=1;
	void SetLineColor(int col);
	int fcol = 0; // 0 = no fill
	void SetFillColor(int fcol);

	
	int w=1;
	void SetLineWidth(int w);

	int style = 1;
	void SetLineStyle(int style);
	int fstyle = -1; // -1 = no fill
	void SetFillStyle(int fstyle);

	void Draw(TCanvas &c, Graphics &g);
	void Delete();
};


//=================
void  Draw_histo(TCanvas &c, Graphics &g, vector<double> &V,const char * xtitre,const char * ytitre, double xmin, double xmax,  double ymin, double ymax, const char * titre, int i0=0);
