#include "Source/editor/manager.h" 
#include "Source/com.h" // commands GUI
#include "Source/processor/PluginProcessor.h"
#include "Source/editor/PluginEditor.h"

#include <sstream>


/*!=========================
 * Constructeur
 */
Manager::Manager()
{
}


//================
Manager::~Manager()
{
}

//===================
void Manager::Loop_manager()
{

	if(changes_MM.load()  == true)  // ask to refresh display
	{
		//	cout<<"ask to refresh s_MM"<<endl;
		p_com->Met_a_jour_Manager_s_MM(); // refresh display
//		p_com->Manager_s_MM_code.moveCaretToEnd(true);
		changes_MM.store(false); // done
	}

	
	if(processor->changes_Lx_copy.load()  == true)  // ask to refresh display
	{
		p_com->Manager_Dessin->repaint();
		processor->changes_Lx_copy.store(false); // done
	}

}


//======================
void Manager::Dessin(juce::Graphics& g)
{
	c.cx = p_com->Manager_Dessin->getX();
	c.cy = p_com->Manager_Dessin->getY();
	c.wx = p_com->Manager_Dessin->getWidth();
	c.wy = p_com->Manager_Dessin->getHeight();	
	g.fillAll(Colour((uint)kBlack));
//	c.Range(-2, -10, 2, 10); // x1,y1,x2,y2 for Vanderpol 
	c.Range(-15, -15, 15, 15); // x1,y1,x2,y2 for Rossler

	//-----Draw the trajectory--------------
	for(int j=0; j< processor->L_x1_copy.size(); j++)
	{
		TMarker m(processor->L_x1_copy[j], processor->L_x2_copy[j], 3);
		m.SetMarkerColor(kYellow);
		m.Draw(c, g);
	}
}
