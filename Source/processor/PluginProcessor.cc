/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "Source/processor/PluginProcessor.h"
#include "Source/editor/PluginEditor.h"
#include "Source/editor/manager.h"
using namespace juce;



#include <chrono>
using namespace std::chrono;
#include <thread>
using namespace std::this_thread; 


//======================================================
Processor::Processor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::mono(), true) // @@ put stereo or  mono?
//					   .withInput("Sidechain", AudioChannelSet::mono(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::mono(), true)
                     #endif
                       )
#endif

{

	manager = new Manager();
	manager->processor = this;

//---- Parameters -----
   	params = new Parameters(manager); // declared in com.h, look to respect the order
	params->Add_Automation_Parameters(this); //in com.cc


//------------
	Mes.reserve(10);
	t0 = high_resolution_clock::now(); // measure of time 
}

//==============================================================================
Processor::~Processor()
{

   //.... for the thread GUI ...
	delete params;
	params = nullptr;

}

//==============================================================================
const juce::String Processor::getName() const
{
    return JucePlugin_Name;
}

//==============================================================================
bool Processor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

//==============================================================================
bool Processor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}
//==============================================================================
bool Processor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}
//==============================================================================
double Processor::getTailLengthSeconds() const
{
    return 0.0;
}
//==============================================================================
int Processor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}
//==============================================================================
int Processor::getCurrentProgram()
{
    return 0;
}
//==============================================================================
void Processor::setCurrentProgram (int index)
{

}
//==============================================================================
const juce::String Processor::getProgramName (int index)
{
    return {};
}
//==============================================================================
void Processor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void Processor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..
	
}
//==============================================================================
void Processor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}
//==============================================================================
// ref: https://docs.juce.com/master/tutorial_audio_bus_layouts.html
#ifndef JucePlugin_PreferredChannelConfigurations
bool Processor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
    return true;
	
}
#endif

//==================================
void   Processor::Print_Midi_Messages(MidiBuffer&  midi_buf)
{
	if(midi_buf.getNumEvents() == 0)
		return;
	
	if(manager == nullptr)
		return;

	//------------- prepare the output
	
	ostringstream s;
	s<<"----------------------\n"<<endl;
	s<<"list of "<< midi_buf.getNumEvents() <<" messages:"<<endl;


	for (const MidiMessageMetadata &metadata : midi_buf) // loop on input midi messages
	{
		MidiMessage mes = metadata.getMessage();
//		s<<"mes.getDescription ()="<<mes.getDescription()<<endl;
//		s<<"mes.size ="<<mes.getRawDataSize()<<endl;

		const uint8* p_mes =  mes.getRawData();
		Mes.assign(p_mes, p_mes + mes.getRawDataSize());
		for(int i=0; i<Mes.size(); i++)
			//s<< hex << setw(2) << setfill('0')<<Mes[i]<<",";
			s<< hex <<(int)Mes[i]<<",";
		//double t = mes.getTimeStamp();
		double t = Date_from_start_in_sec();

		s<<"\tt="<< t; // time from start 
		s<<endl;
	
	}

//	cout<<s.str(); 
	
	if(manager->mtx_s_MM.try_lock())
	{
		size_t N = manager->s_MM.size();
		if(N > 10000) // if string too large,
			manager->s_MM.erase(0, N - 9000);  // we let the last 9000
			
		manager->s_MM.append(s.str());
		manager->changes_MM.store(true);  // ask to refresh display in manager.cc
		manager->mtx_s_MM.unlock();
	}
	
}

//===========================
// input: k key MIDI value
// output: f frequency 
double key_to_f(int k)
{
	return 440. * pow(2., (k - 69.)/12.);
}


//==========================
// input: k key MIDI value
// ouput:  index of the value k in vector L if found,
//         -1 if not found.
int Is_note_in_the_list(int k, vector<int>& L)
{
    auto it = std::find(L.begin(), L.end(), k);
    return (it != L.end()) ? std::distance(L.begin(), it) : -1;
}
	

//==============================================================================
/*

 */

void Processor::processBlock(AudioBuffer<float>& audio_buffer, MidiBuffer& midi_buffer)
{
	Print_Midi_Messages(midi_buffer); // if needed
	double t_begin = Date_from_start_in_sec(); // time to measure latency

	
	//----- Loop over input MIDI messages
	for (const MidiMessageMetadata &metadata : midi_buffer)
	{
	
		MidiMessage message = metadata.getMessage();
		
		if (message.isNoteOn())
		{
			int c = message.getChannel() - 1; // shift channel
			int k =  message.getNoteNumber();
			int v =  message.getVelocity();


			if(Is_note_in_the_list(k, L_k) == -1) // note is not in the list
			{
				L_k.push_back(k); // add k to the list L_k
				L_v.push_back(v);
				L_t.push_back(0);
				L_f.push_back(key_to_f(k));
				L_x1.push_back(0.1);
				L_x2.push_back(0);
				L_x3.push_back(0);
				L_tau.push_back(0); // time from start of note
				L_T_tau.push_back(2*M_PI); // period of the cycle
				L_tau_cross_old.push_back(0); // date of cross section
				i_copy = 0;
			}
		
		}
		else if (message.isNoteOff())
		{
			int c = message.getChannel() - 1; // shift channel
			int k = message.getNoteNumber();
			int v = message.getVelocity();

			int pos = Is_note_in_the_list(k, L_k);
			if(pos >= 0) // note is  in the list
			{
				L_k.erase(L_k.begin() + pos); //remove element at position pos in the list L_k
				L_v.erase(L_v.begin() + pos);
				L_t.erase(L_t.begin() + pos);
				L_f.erase(L_f.begin() + pos);
				L_x1.erase(L_x1.begin() + pos);
				L_x2.erase(L_x2.begin() + pos);
				L_x3.erase(L_x3.begin() + pos);
				L_tau.erase(L_tau.begin() + pos);
				L_T_tau.erase(L_T_tau.begin() + pos);
				L_tau_cross_old.erase(L_tau_cross_old.begin() + pos);
			}
		}
	}

	

	//-----for editor --------------------
	if(L_x1_copy.size() != N_tau)
		{
			L_x1_copy.resize(N_tau);
			L_x2_copy.resize(N_tau);
			L_x3_copy.resize(N_tau);
		}
	
	//--- Audio parameters
	int N = getSampleRate(); // nombre echantillons / sec.
	int n = audio_buffer.getNumSamples(); // nombre echantillons dans le buffer
	int nchan = audio_buffer.getNumChannels(); // 1: mono, 2: stereo


    //.......fill audio buffer ...........
	
	int ch = 0; // channel, mono
	float* bufferPtr = audio_buffer.getWritePointer(ch); // pointeur du buffer,

	for (int i = 0; i< n; i++) // audio samples
	{
		bufferPtr[i] = 0; // init buffer
		for(int ik = 0; ik < L_k.size(); ik++) // loop on active notes
		{
			double f = L_f[ik];
			double T_tau = L_T_tau[ik]; // measured period of the cycle
			double E = 3; // exponent
			
			double mu = pow(L_v[ik] / 127., E) * 10. + 0.03;  // map v \in [0,127] to mu \in [0,10]
			c = 4  + 16 * pow(L_v[ik] / 127., E); 
			
			int M = floor(N_Euler * f * T_tau / N); // subdivision of time step
			if(M<1)
				M=1;

			double x1 = L_x1[ik], x2 = L_x2[ik], x3 = L_x3[ik];
			double	delta_tau = f*T_tau/(M*N); 
			
			for(int im=0; im<M; im++) // subloop
			{
				//. formula for the VanderPol vector field
				/*
				double V1 = x2;
				double V2 = mu * (1 - x1*x1) * x2 -x1;
				double x2_old = x2;
				x1 = x1 + delta_tau * V1;
				x2 = x2 + delta_tau * V2;
				*/

				//. formula for the Rossler vector field
				double V1 = -x2 -x3;
				double V2 = x1 + a *x2;
				double V3 = b + x3 * (x1 - c);

				double x2_old = x2;
				
				x1 = x1 + delta_tau * V1;
				x2 = x2 + delta_tau * V2;
				x3 = x3 + delta_tau * V3;


				//.. look if we cross the positive axis x1
				
				if(x1 >0 && (x2 * x2_old < 0))
				{
					double tau_cross =  L_tau[ik] + (x2/(x2_old-x2))*delta_tau; // date of cross section

					double T_tau  = tau_cross - L_tau_cross_old[ik]; // measured period
					if(T_tau < 0.1)
						T_tau = 0.1;
					L_T_tau[ik] = T_tau; //memorize period
					L_tau_cross_old[ik] = tau_cross; // memorize date of cross section
				} // if
				
				L_tau[ik] += delta_tau; // increment effective time

                //... copy to the editor
				if(ik == (L_k.size()-1)) // consider last note only
				{
					if(changes_Lx_copy.load()  == false) // editor needs a new copy
					{
						L_x1_copy[i_copy]= x1;
						L_x2_copy[i_copy]= x2;
						L_x3_copy[i_copy]= x3;
						i_copy++;
					}

					if(i_copy > N_tau) // end of the copy
					{
						i_copy = 0;
						changes_Lx_copy.store(true); // copy is done
					}
				}
			} // for im

			L_x1[ik] = x1; // memorizes
			L_x2[ik] = x2;
			L_x3[ik] = x3;

			double A = 0.02 * (L_v[ik] / 127.);
			bufferPtr[i] += A* L_x1[ik];			
		} // for ik
	} // for i
	   
	   


	

	//====  measures time and latency ===================

	double t_end = Date_from_start_in_sec(); // time , end of block process

	double dt = audio_buffer.getNumSamples() / double( getSampleRate());  // mean duration of a block


	//... max  latency ....
	double latency =  (t_end - t_begin) / dt; // ratio

	if(manager != nullptr && mtx.try_lock())
	{

		if(latency > manager->latency)
			manager->latency = latency;

		//... mean latency
		manager->N_latency++;
		manager->S_latency += (t_end - t_begin);
	
		if(manager->N_latency >= 1000)
		{
			manager->latency_mean =  manager->S_latency / (1000. * dt);
			manager->N_latency = 0;
			manager->S_latency = 0;
		}
		mtx.unlock();
	}
}

//==============================================================================
bool Processor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}
//==============================================================================
juce::AudioProcessorEditor*  Processor::createEditor()
{
    return new Editor (*this);
}

//==============================================================================
void Processor::getStateInformation (MemoryBlock& destData)
{
	params->Save_parameters(this, destData); // in com.cc
}
//==============================================================================
void Processor::setStateInformation (const void* data, int sizeInBytes)
{
	params->Load_parameters(this, data, sizeInBytes); // in com.cc
}


//==================================
/*
This function is called if a parameter is changed: from automation or by the plugin.
 */
void  Processor::parameterValueChanged (int parameterIndex, float newValue) 
{
	params->Transmit_Automation_Parameters(this, parameterIndex); // in com.cc
}

//=======================
void  Processor::parameterGestureChanged(int parameterIndex, bool gestureIsStarting)
{
}



//========================
double 	Processor::Date_from_start_in_sec() //date from t0 in sec.
{
	auto t2 = high_resolution_clock::now(); // measure of time 
	return 		duration_cast<duration<double>>(t2 - t0).count(); // duration in sec.
}

//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Processor();
}
